import environ
import requests
import json

from rest_framework import status

env = environ.Env()


def get_airpollution_data_by_latlon(lat, lon):
    url = f"{env('BASE_WEATHER_URL')}?lat={lat}&lon={lon}&appid={env('BASE_WEATHER_API_KEY')}"

    response = requests.request("GET", url)

    if response.status_code == status.HTTP_200_OK:
        return json.loads(response.text)
    return False
