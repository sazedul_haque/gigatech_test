from django.db import models

from core.models import BaseModel


class Address(BaseModel):
    street = models.CharField(
        max_length=512,
        null=True,
        blank=True
    )
    city = models.CharField(
        max_length=30,
        null=True,
        blank=True
    )
    state = models.CharField(
        max_length=10,
        null=True,
        blank=True
    )
    zip_code = models.CharField(
        max_length=10,
        null=True,
        blank=True
    )

    def __str__(self):
        return self.full_address

    class Meta:
        ordering = ['-created_at']

    @property
    def full_address(self):
        street = f" {self.street}," if self.street else ''
        zip_code = f" {self.zip_code}," if self.zip_code else ''
        city = f" {self.city}," if self.city else ''
        state = f" {self.state}" if self.state else ''
        return f"{street}{zip_code}{city}{state}"


class User(BaseModel):
    first_name = models.CharField(
        max_length=150,
        blank=True
    )
    last_name = models.CharField(
        max_length=150,
        blank=True
    )
    parent = models.ForeignKey(
        to='self',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    address = models.ForeignKey(
        to='Address',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        ordering = ['-created_at']
