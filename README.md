
---

## Task details

#### About (Story)
You can choose one of the following API, but are not limited to them.
● OpenWeatherMap APIs
● Freecurrencyapi APIs
1. Create a django app using one of the apis.
2. Apply CRUD operation and write appropriate rest Apis based on your application.
3. Create an interface to do these CRUD operations.
4. Use postgresql or MySQL database to store the data.
5. Use the default user model to store all types of user and admin information.
6. Handle all the errors using custom exception class and show the appropriate messages
through the api.
7. Create a module that can convert an English number to Bangla number in words and also
use it in your projects.
*Don’t use any library for the conversion of numbers to words


[comment]: <> (All standards must be respected)

### Requirements for implementation:
You can choose one of the following API, but are not limited to them.
● OpenWeatherMap APIs
● Freecurrencyapi APIs
1. Create a django app using one of the apis.
2. Apply CRUD operation and write appropriate rest Apis based on your application.
3. Create an interface to do these CRUD operations.
4. Use postgresql or MySQL database to store the data.
5. Use the default user model to store all types of user and admin information.
6. Handle all the errors using custom exception class and show the appropriate messages
through the api.
7. Create a module that can convert an English number to Bangla number in words and also
use it in your projects.
*Don’t use any library for the conversion of numbers to words
Example :

1500 ⇒ One Thousand Five Hundred

8. Also select one or more areas where you are most confident and show us quality design and
code.
9. Try to show us as far as possible - how well you know about django in this short time.
(hint: decorators, cron job, caching, signals, customize api permission)
10. Add readme file for - how to run the application and API documentation.
11. Also you can dockerized your application if you are familiar with docker.




### Solution built using: 

coord Coordinates from the specified location (latitude, longitude)
list
dt Date and time, Unix, UTC
main
main.aqi Air Quality Index. Possible values: 1, 2, 3, 4, 5. Where 1 = Good, 2 = Fair, 3 = Moderate, 4 = Poor, 5 = Very Poor.
components
components.co Сoncentration of CO (Carbon monoxide), μg/m3
components.no Сoncentration of NO (Nitrogen monoxide), μg/m3
components.no2 Сoncentration of NO2 (Nitrogen dioxide), μg/m3
components.o3 Сoncentration of O3 (Ozone), μg/m3
components.so2 Сoncentration of SO2 (Sulphur dioxide), μg/m3
components.pm2_5 Сoncentration of PM2.5 (Fine particles matter), μg/m3
components.pm10 Сoncentration of PM10 (Coarse particulate matter), μg/m3
components.nh3 Сoncentration of NH3 (Ammonia), μg/m3


### Solution built using: 

* Django and Python3, 
* Django Rest Framework, 
* SQLite3

### How to run: 

First we need to clone it from following repo:
- https://bitbucket.org/sazedul_haque/gigatech_test/src/master/

Here we included the db in our repository which is ``ud_test.sqlite3``
and also added `/conf/.env` file as it is a test project

You need to install python 3.9 [python download link](https://www.python.org/downloads/) 
After cloning it from repository you need to create run following command to install required packages.

```
pip install -r requirements.txt
```

```
python manage.py migrate
python manage.py runserver
```



### URL for Air pollution data
Using this url we can get list, create, update, delete Air pollution data
```
GET/POST | http://127.0.0.1:8000/api/v1/air-pollution/
PUT/PATCH/DELETE | http://127.0.0.1:8000/api/v1/air-pollution/2(id)
```
### URL for inserting Air pollution data from API
Here you to provide the lati
Using this url we can get list, create, update, delete Air pollution data
```
POST | http://127.0.0.1:8000/api/v1/insert-air-pollution-data/
```

Example of how you can insert data from Weather API Using CURL

```
curl --location --request POST 'http://127.0.0.1:8000/api/v1/insert-air-pollution-data/' \
--header 'Content-Type: application/json' \
--data-raw '{
    "lat": 23.684994,
    "lon": 90.356331
}'
```




