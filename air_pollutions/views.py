import logging

from rest_framework import filters as rest_filters
from rest_framework import permissions, viewsets, pagination, status
from rest_framework.response import Response
from rest_framework.views import APIView

from air_pollutions import serializers, models
from core.services.weather_air_pollution_api import get_airpollution_data_by_latlon

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class AirPollutionBaseViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    filter_backends = (rest_filters.SearchFilter, rest_filters.OrderingFilter)
    pagination_class = pagination.LimitOffsetPagination
    ordering_fields = ['-created_at']
    lookup_field = 'id'


class AirPollutionViewSet(AirPollutionBaseViewSet):
    serializer_class = serializers.AirPollutionSerializer
    queryset = serializer_class.Meta.model.objects.all()


class InsertAPDATA(APIView):
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        """
        get and insert air pollution data
        :param request:
        :return:
        """

        request_data = self.request.data or self.request.POST or self.request.GET

        data = get_airpollution_data_by_latlon(request_data.get("lat"), request_data.get("lon"))
        components = data.get("list")[0].get("components")
        aqi = data.get("list")[0].get("main")
        dt = data.get("list")[0].get("dt")

        models.AirPollution.objects.create(
            lat=request_data.get("lat"),
            lon=request_data.get("lon"),
            aqi=aqi.get("aqi"),
            co=components.get("co"),
            no=components.get("no"),
            no2=components.get("no2"),
            o3=components.get("o3"),
            so2=components.get("so2"),
            pm2_5=components.get("pm2_5"),
            pm10=components.get("pm10"),
            nh3=components.get("nh3"),
            dt=dt,
        )

        return Response(data, status=status.HTTP_200_OK)
