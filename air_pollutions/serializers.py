from rest_framework import serializers

from air_pollutions import models


class AirPollutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AirPollution
        read_only_fields = ('id',)
        fields = read_only_fields + (
            "lat",
            "lon",
            "aqi",
            "co",
            "no",
            "no2",
            "o3",
            "so2",
            "pm2_5",
            "pm10",
            "nh3",
            "dt",
        )
        extra_kwargs = {'url': {'lookup_field': 'id'}}
