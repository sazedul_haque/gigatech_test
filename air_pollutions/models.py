from django.db import models

from core.models import BaseModel


class AirPollution(BaseModel):
    lat = models.DecimalField(
        verbose_name='latitude',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='latitude',
    )
    lon = models.DecimalField(
        verbose_name='longitude',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='longitude',
    )
    aqi = models.IntegerField(
        verbose_name='Air Quality index',
        null=True,
        blank=True,
        help_text='Air Quality index',
    )
    co = models.DecimalField(
        verbose_name='co',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='co',
    )
    no = models.DecimalField(
        verbose_name='no',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='no',
    )
    no2 = models.DecimalField(
        verbose_name='no2',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='no2',
    )
    o3 = models.DecimalField(
        verbose_name='o3',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='o3',
    )
    so2 = models.DecimalField(
        verbose_name='so2',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='so2',
    )
    pm2_5 = models.DecimalField(
        verbose_name='pm2_5',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='pm2_5',
    )
    pm10 = models.DecimalField(
        verbose_name='pm10',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='pm10',
    )
    nh3 = models.DecimalField(
        verbose_name='nh3',
        max_digits=20,
        decimal_places=6,
        null=True,
        blank=True,
        help_text='nh3',
    )
    dt = models.IntegerField(
        verbose_name='dt',
        null=True,
        blank=True,
        help_text='dt',
    )

    def __str__(self):
        return f"{self.lat} {self.lon}"

    class Meta:
        ordering = ['-created_at']
