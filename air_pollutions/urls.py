from django.urls import path
from rest_framework import routers

from air_pollutions import views

router = routers.SimpleRouter()
router.register('v1/air-pollution', views.AirPollutionViewSet, basename="air-pollution")

urlpatterns = [
    path('v1/insert-air-pollution-data/', views.InsertAPDATA.as_view()),
]

urlpatterns += router.urls
